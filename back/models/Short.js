const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ShortUrlSchema = new Schema({
  originalUrl: {
    type : String,
    required: true
  },
  short: {
    type: String,
    required: true,
    unique: true
  }
});

const Short = mongoose.model('Short', ShortUrlSchema);

module.exports = Short;