const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const nanoid = require('nanoid');
const Short = require("./models/Short");

const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect('mongodb://localhost/short', {useNewUrlParser: true}).then(() => {

  app.post('/', (req, res) => {
    const original = req.body.originalUrl;
    const originalUrl = new Short({originalUrl: original, short: nanoid(6)});
    originalUrl.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  app.get('/:short', (req, res) => {
    Short.findOne({short: req.params.short})
      .then(original => {
        if (original) res.status(301).redirect(original.originalUrl);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
