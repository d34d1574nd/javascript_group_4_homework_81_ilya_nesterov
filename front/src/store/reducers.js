import {FETCH_LINK_SUCCESS} from "./actions";

const initialState = {
  link: {},
};

const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LINK_SUCCESS:
      return {...state, link: action.link};
    default:
      return state;
  }
};

export default productsReducer;