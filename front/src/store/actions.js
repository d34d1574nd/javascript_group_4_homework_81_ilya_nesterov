import axios from '../axios-api';

export const FETCH_LINK_SUCCESS= 'FETCH_LINK_SUCCESS';

export const fetchLinkSuccess = link => ({type: FETCH_LINK_SUCCESS, link});

export const createLink = link => {
  return dispatch => {
    return axios.post('/', link).then(
      response => dispatch(fetchLinkSuccess(response.data))
    );
  };
};
