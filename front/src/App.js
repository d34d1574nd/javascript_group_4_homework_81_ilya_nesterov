import React, { Component } from 'react';
import './App.css';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {createLink} from "./store/actions";

class App extends Component {
  state = {
    originalUrl: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.createLink({...this.state});
  };

  render() {
    return (
      <div className="App">
        <Form onSubmit={this.submitFormHandler}>
          <FormGroup>
            <Label for="exampleText">Shorten your link</Label>
            <Input
              type="text" name="originalUrl"
              id="exampleText"
              placeholder="Enter your link"
              value={this.state.originalUrl}
              onChange={this.inputChangeHandler}
            />
          </FormGroup>
          <Button type="submit" color="primary" outline >Shorten</Button>
        </Form>
        <div className="link" >
          {this.props.link.short ? <a href={this.props.link.originalUrl}>http://localhost:8000/{this.props.link.short}</a> : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  link: state.link
});

const mapDispatchToProps = dispatch => ({
  createLink: link => dispatch(createLink(link)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
